=========
Resources
=========

These were influential docs in the creation of this template.

* `Python's New Package Landscape <http://andrewsforge.com/article/python-new-package-landscape/>`_ 2018-05-11 by Andrew Pinkham
* `Packaging a python library, The Structure <https://blog.ionelmc.ro/2014/05/25/python-packaging/#the-structure>`_ 2014-05-25 by Ionel Cristian Mărieș
* `Testing & Packaging <https://hynek.me/articles/testing-packaging/>`_ 2015-10-19 by Hynek Schlawack

* `Inside the Cheeseshop: How Python Packaging Works <https://www.youtube.com/watch?v=AQsZsgJ30AE>`_ PyCon 2018 by Dustin Ingram
