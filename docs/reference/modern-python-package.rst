=======================
A Modern Python Project
=======================

Here we discuss the rational of using `Pipfile`_ and `PEP-518`_ and not `PEP-517`_.

******
Pipenv
******

`Pipenv`_ and the `Pipfile`_ provide a much better user experience by handling the environment and dependencies while maintaining compatibility with traditional workflows. With that said, we are not using Pipenv to generate a requirements.txt file, but it is possible.


*******
PEP-518
*******

`PEP-518`_ gives us a way to declare dependencies before running `setup.py` with `pyproject.toml`.


*******
PEP-517
*******

`PEP-517`_ allows us to replace `setuptools` with something like `Flit` or `Poetry`. While these projects can provide a more streamline experience, they have their own workflows may not be friendly towards new contributors to a project.


.. _Pipfile: https://github.com/pypa/pipfile
.. _Pipenv: https://github.com/pypa/pipenv
.. _PEP-518: https://www.python.org/dev/peps/pep-0518/
.. _PEP-517: https://www.python.org/dev/peps/pep-0517/
