""" __main__ to load package {{ cookiecutter.project_name }} as module """
import logging
import sys


def main():
    """
    Main entry point.
    """
    logging.debug("Arguments received: %s", sys.argv[1:])


if __name__ == "__main__":
    main()
