===================
cookiecutter-python
===================

A Cookiecutter_ template for Python projects

.. _cookiecutter: https://github.com/audreyr/cookiecutter

**********
Quickstart
**********

Install the latest Cookiecutter if you haven't installed it yet (this requires
Cookiecutter 1.6.0 or higher):

.. code-block:: bash

    pip install -U 'cookiecutter<=1.6'

Generate a Python package project

.. code-block:: bash

    cookiecutter https://github.com/digitalr00ts/cookiecutter-python.git

********
Features
********

* Python package src-layout
* Pipfile
* PEP-518 pyproject.toml

*****
To Do
*****

* Cleanup documentation
* Add Apache license option
* Add licenses for documentation projects
   * Creative Commons Attribution
   * Creative Commons Zero
   * Creative Commons Attribution Share Alike
   * GNU Free Documentation License (FDL)
   * Public Documentation License (PDL)
   * FreeBSD Documentation License
   * Open Publication License
